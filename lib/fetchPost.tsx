import fetchData from '@/lib/fecthData'

export default async function posts(
  slug: string
){

  const opt = '' +
    `slug=${slug}&` +
    `_fields=`+
      `id,modified_gmt,date_gmt,slug,title,link,`+
      `content,yoast_head_json,jetpack_featured_media_url`
  ;

  const prefix = process.env.WP_API
  const { data } = await fetchData(`${prefix}/posts?${opt}`)
  return data[0]

}