import fetchData from '@/lib/fecthData'

export default async function posts(
  page: number = 1,
  perPage: number = 3
){

  const prefix = process.env.WP_API
  const opt = '' +
    `per_page=${perPage}&` +
    `page=${page}&` +
    `_fields=id,modified_gmt,date_gmt,slug,title,link,excerpt,jetpack_featured_media_url`
  ;

  const data = await fetchData(
    `${prefix}/posts?${opt}`,
    (headers) => ({
      'x-wp-total': headers.get('x-wp-total'),
      'x-wp-totalpages': headers.get('x-wp-totalpages')
    })
  )

  return {
    page, 
    perPage,
    totalPosts: data.headers['x-wp-total'],
    totalPages: data.headers['x-wp-totalpages'],
    posts: data.data
  }

}