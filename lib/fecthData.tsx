import 'server-only'

// a lib to fetch data once

let cache:{[key:string]: any} = {}

export default async function fetchData(
  url: string,
  getHeaders?: (headers:any) => any
){

  if(cache[url]) return await cache[url]

  cache[ url ] = fetch(url).then(async res => ({
    headers: getHeaders ? getHeaders(res.headers) : {},
    data: await res.json()
  }))

  return await cache[ url ]

}