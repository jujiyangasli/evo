

export default interface Post{
  id: string,
  date_gmt: string,
  modified_gmt: string,
  slug: string,
  link: string,
  title: {
    rendered: string
  },
  content: {
    rendered: string
  },
  jetpack_featured_media_url: string
  yoast_head_json: any
}