

export default interface PostExcerpt{
  id: string,
  date_gmt: string,
  modified_gmt: string,
  slug: string,
  link: string,
  title: {
    rendered: string
  },
  excerpt: {
    rendered: string
  },
  jetpack_featured_media_url: string
}