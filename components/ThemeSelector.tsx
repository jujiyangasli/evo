'use client'

import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { FiSun, FiMoon } from "react-icons/fi";

const Container = styled.div`
  display: flex;
  button{
    border: 0px;
    background-color: transparent;
    color: var(--text);
    font-size: 1.6rem;
    cursor: pointer;
    line-height: 100%;
    padding: 0 21px;
  }
`

const KEY = 'darkmode'


// to prevent flicker
export const InitScript = () => <script dangerouslySetInnerHTML={{
  __html: `
  if(
    typeof localStorage !== 'undefined' &&
    typeof window !== 'undefined' &&
    typeof window.matchMedia !== 'undefined' &&
    typeof document !== 'undefined' &&
    typeof document.querySelector !== 'undefined'
  ){
    const val = localStorage.getItem('${KEY}');
    const dark = val === '1' ? true : val === '0' ? false :
      !!window.matchMedia('(prefers-color-scheme:dark)').matches;
      document.querySelector('html')?.classList.add(dark ? 'dark' : 'light');
  }
  `
}} />

export default function ThemeSelector(
  { className }:
  { className?: string }
){

  const [dark, setDark ] = useState<boolean|null>(null)

  useEffect(() => {
    setDark(
      !!document.querySelector('html.dark')
    )
  },[])

  const setDarkMode = (darkMode: boolean) => {
    setDark(darkMode)
    localStorage.setItem(KEY, darkMode ? '1' : '0')
    document.querySelector('html')?.classList.remove(darkMode ? 'light' : 'dark')
    document.querySelector('html')?.classList.add(darkMode ? 'dark' : 'light')
  }

  return <Container className={className}>
    {dark === null ? <button>...</button> :
      dark ? <button 
        title="Change to light mode"
        onClick={() => setDarkMode(false)}><FiSun /></button> :
      <button 
        title="Change to dark mode"
        onClick={() => setDarkMode(true)}><FiMoon /></button>}
  </Container>

}