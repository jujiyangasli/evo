
import { PostStyled, ContentStyled} from './style'
import Link from 'next/link'
import HtmlContent from '@/components/HtmlContent';
import type Post from '@/types/Post'
import { format } from 'date-fns'

export default function PostPage({ post }:{ post:Post }){

  return <PostStyled>
    {/* <div>
      <pre>
        {JSON.stringify(post,null,2)}
      </pre>
    </div> */}
    <h1>{post.title.rendered}</h1>
    <div className="meta">
      <p>{format(new Date(post.modified_gmt), 'PPPppp')}</p>
      <Link href="/">◀ home</Link>
    </div>
    <div className="image">
      <img 
        src={post.jetpack_featured_media_url}
        alt={post.title.rendered}
      />
    </div>
    <ContentStyled className="content">
      <HtmlContent content={post.content.rendered} />
    </ContentStyled>
  </PostStyled>

}

