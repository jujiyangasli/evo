'use client';

import styled from 'styled-components'

export const PostStyled = styled.article`

  max-width: 800px;
  margin: 0 auto;

  &>h1{
    text-align: left;
    margin: 5rem auto;
    font-size: 5rem;

    @media (max-width: 569px) {
      font-size: 3rem;
      margin: 3rem auto;
    }
  }

  

  .image{
    img{
      max-width: 100%;
      margin: 0 auto;
      display:block;
    }
  }

  .meta{
    display: flex;
    margin-bottom: 2rem;
    justify-content: space-between;

    p{
      color: var(--muted);
      font-size: small;
    }

    @media (max-width: 538px) {
      flex-direction: column-reverse;

      a{
        margin-bottom: 1rem;
      }
    }
  }
 
`

export const ContentStyled = styled.div`

  line-height: 1.6rem;

  p{
    margin: 1rem 0;
  }
  
  h1, h2, h3, h4, h5, h6{
    text-align: left;
    margin: 1rem auto;
  }

  img{
    max-width: 100%;
    height: auto;
    margin: 1rem auto;
  }

  ul,ol{
    margin-left: 1rem;
  }
`