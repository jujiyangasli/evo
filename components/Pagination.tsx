'use client';
import { useMemo } from 'react'
import Link from 'next/link'
import styled from 'styled-components'

const StyledPagination = styled.nav`

  display: flex;
  justify-content: center;

  ul,li{
    list-style: none;
  }

  li{
    display: inline-block;
  }

  a{
    text-decoration: none;
    color: var(--text);
    padding: 1rem 1rem;

    &.active{
      color: var(--anchor);
      font-weight: bold;
    }
  }
  
`

// basis 1 pagination
export default function Pagination({ 
  pageCount,
  initialPage,
  maxLength = 3
}:{
  pageCount:number
  initialPage: number
  maxLength?: number
}){

  const totalPage = useMemo(() => {
    return [...Array(pageCount)].map((v,i) => i+1)
  },[ pageCount ])

  const pages = useMemo(() => {
    
    const currentIdx = totalPage.indexOf(initialPage)
    let ret = []
    
    if((currentIdx+1) > maxLength) {
      ret = currentIdx > ((totalPage.length-1)-maxLength) ? 
        totalPage.slice(totalPage.length-maxLength) : 
        totalPage.slice(currentIdx,currentIdx+maxLength)
    }
    else ret = totalPage.slice(0,maxLength)
    
    return ret

  },[ maxLength, totalPage, initialPage ])

  const prevPage = initialPage-1
  const nextPage = initialPage+1

  return <StyledPagination><ul>
    <li>
      <Link
        href={prevPage<=0?`/?page=${initialPage}`:`/?page=${prevPage}`}
        className={prevPage<=0?'disabled':''}
      >Prev</Link>
    </li>

    {pages[0] > totalPage[0] ? <li>
      <Link
        href={`/?page=${pages[0]-1}`}
      >...</Link>
    </li>:null}

    {pages.map((v:number) => (
      <li key={v}>
        <Link
          href={`/?page=${v}`}
          className={v === initialPage?'active':''}
        >{v}</Link>
      </li>
    ))}

    {pages[pages.length-1] < totalPage[totalPage.length-1] ? <li>
      <Link
        href={`/?page=${pages[pages.length-1]+1}`}
      >...</Link>
    </li>:null}

    <li>
      <Link
        href={nextPage>pageCount?`/?page=${initialPage}`:`/?page=${nextPage}`}
        className={nextPage>pageCount?'disabled':''}
      >Next</Link>
    </li>
  </ul></StyledPagination>

}