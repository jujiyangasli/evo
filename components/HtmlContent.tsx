// 'use client'
import { Interweave } from 'interweave';
import { FilterInterface } from 'interweave';
import { polyfill } from 'interweave-ssr';

polyfill()

const linkFilter: FilterInterface = {
  node(name, node) {
    if (name === 'a') {
      node.setAttribute('target', '_blank');
    }

    return node;
  },
};

export default function HtmlContent({ content }:{ content: string }){

  return <Interweave 
    content={content} 
    noWrap={true}
    filters={[linkFilter]}
  />

}