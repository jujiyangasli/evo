
import PostTileStyled from './style'
import Link from 'next/link'
import HtmlContent from '@/components/HtmlContent';
import type PostExcerpt from '@/types/PostExcerpt'

export default function PostTile({ post }:{ post:PostExcerpt }){

  return <PostTileStyled>
    <div className="image">
      <img 
        src={post.jetpack_featured_media_url}
        alt={post.title.rendered}
      />
      <Link 
        href={`/p/${post.slug}`}
        title={post.title.rendered}
        // prefetch={false}
      ></Link>
    </div>
    <div className="excerpt">
      <h2><Link 
        href={`/p/${post.slug}`}
        title={post.title.rendered}
        // prefetch={false}
      >{post.title.rendered}</Link></h2>
      <div className="content">
        <HtmlContent content={post.excerpt.rendered} />
      </div>
    </div>
  </PostTileStyled>

}

