'use client';

import styled from 'styled-components'

const PostTile = styled.article`

  display: block;
  max-width: 500px;
  margin: 2rem auto;
  margin-top: 0rem;
  margin-bottom: 2rem;
  border: 1px solid var(--border);
  padding: 1rem;
  transition: transform 200ms;
  transform: scale(1);
  background-color: var(--bg);

  &:has(.image a:active),
  &:has(.excerpt a:active){
    transform: scale(0.98);
    background-color: var(--activeElm);
  }

  .image{

    position: relative;

    img{
      width: 100%;
    }

    a{
      position: absolute;
      top:0;
      left:0;
      width: 100%;
      height: 100%;
    }
  }

  .excerpt{
    h2 {
      margin: 1rem 0;
      a {
        color: var(--text);

        &:hover{
          color: var(--anchor);
        }
      }
    }

    .content{
      line-height: 1.6rem;
    }
  }
`

export default PostTile