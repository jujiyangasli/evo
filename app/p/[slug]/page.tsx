import fetchPost from '@/lib/fetchPost'
import { Metadata, ResolvingMetadata } from 'next';
import PostContent from '@/components/PostContent'

interface Props {
  params: { slug: string }
}

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  
  // read route params
  const post = await fetchPost(params.slug)
 
  // optionally access and extend (rather than replace) parent metadata
  const par = await parent
  const images = [
    ...(par ? par.openGraph?.images || [] : []),
    ...(post.yoast_head_json.og_image||[])
  ];
 
  return {
    title: post.yoast_head_json.title,
    description: post.yoast_head_json.og_description,
    alternates: {
      canonical: post.yoast_head_json.canonical
    },
    openGraph: {
      title: post.yoast_head_json.title,
      description: post.yoast_head_json.og_description,
      url: post.yoast_head_json.og_url,
      siteName: post.yoast_head_json.og_site_name,
      images: images,
    },
    twitter: {
      card: post.yoast_head_json.twitter_card,
      title: post.yoast_head_json.title,
      description: post.yoast_head_json.og_description,
      images: images.map((v:{url:string}) => v.url),
    },
  };
}

async function PostPage({ params }: Props) {
  
  const post = await fetchPost(params.slug)

  return <PostContent post={post} />
}

export default PostPage as unknown as () => JSX.Element;