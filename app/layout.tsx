import './globals.scss'
import { Inter } from 'next/font/google'
import { Metadata } from 'next';
import Link from 'next/link'
import StyledComponentsRegistry from '@/lib/registry';
import ThemeSelector, { InitScript } from '@/components/ThemeSelector';
// import Script from 'next/script'

// import dynamic from 'next/dynamic'
// const ThemeSelector = dynamic(
//   () => import('@/components/ThemeSelector'),
//   { ssr: false }
// )

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'EVO: Blog',
  description: 'An Example blog for EVO',
  viewport: {
    width: 'device-width',
    initialScale: 1
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {


  return (
    <html lang="en">
      <head>
        <InitScript />
      </head>
      <body className={inter.className}>
        <StyledComponentsRegistry>
          <header className='main-header'>
            <h1><Link className="logo" 
              href="/" title="EVO: Blog">EVO: Blog</Link></h1>
            <ThemeSelector className="dark-mode-selector" />
          </header>
          <main>
            {children}
          </main>
        </StyledComponentsRegistry>
      </body>
    </html>
  )
}
