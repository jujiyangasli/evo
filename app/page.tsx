import fetchPosts from '@/lib/fetchPosts'
import Pagination from '@/components/Pagination';
import PostTile from '@/components/PostTile';
import type PostExcerpt from '@/types/PostExcerpt'


type Props = {
  params: {}
  searchParams: { page?: string }
}

async function Home(params: Props) {
  
  const page = Number(params.searchParams?.page)
  const data = await fetchPosts(page||1)

  return <div>
    <div>
      {data.posts.map((post:PostExcerpt) => <PostTile key={post.id} post={post} />)}
    </div>
    <Pagination 
      initialPage={data.page} 
      pageCount={Number(data.totalPages)} 
    />
  </div>
}

export const dynamic = 'force-dynamic'
export default Home as unknown as () => JSX.Element;